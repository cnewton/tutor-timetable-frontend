# AngularJS Tutor Timetable Development
---

This Readme will guide you through the process of setting up an environment for developing the Angular Tutor Timetable interface.

## AngularJS Framework
---
[AngularJS][1] is an open-source web application framework mainly maintained by Google. It aims to simplify both the development and the testing of web applications by providing a framework for client-side model–view–controller (MVC) architecture.  

The AngularJS framework works by first reading the HTML page, which has embedded into it additional custom tag attributes. Angular interprets those attributes as directives to bind input or output parts of the page to a model that is represented by standard JavaScript variables. The values of those JavaScript variables can be manually set within the code, or retrieved from static or dynamic JSON resources.

## Development
---

### Node.js, NPM and Bower
1. Download and install the latest version of [Node.js][2] - Ensure you include NPM with the installation.

2. On a command line enter "npm install npm -g" You may need elevated priveleges.

3. Again on a command line enter "npm install bower -g"

### Cloning the project
1. Create a new folder in your preferred working directory.

2. Using your preferred git tool, clone the repository https://cnewton@bitbucket.org/cnewton/tutor-timetable-frontend.git		

3. Checkout a local copy of the origin/master branch

### Project Setup
1. Navigate to the root directory of the newly cloned repo.

2. On the command line enter "npm install"

3. Again on the command line enter "bower install"

### Development
1. To download new packages for use in the project use "bower install package-namerea"

2. To include these packages in the build file use "bower install package-name --save"

3. Add the packages to the application by including them in the appropriate section of the index.html file.

### Running a temporary webserver
1. To run the webserver enter "grunt serve" into the command line

2. Changes made to the source files (other than css) while grunt is serving will trigger a livereload of the web page.

### Deploying a webserver
1. Obtain and prepare your preferred web page serving system. [Nginx][3] has been used with success before.

2. Point the server software at the tutor-timetable-frontend/dist directory

3. Enter "grunt build" into the command line.

4. The website should be accessible like any other website.

[1]: https://angularjs.org/ "AngularJS"
[2]: https://nodejs.org/en/ "NodeJS"
[3]: https://nginx.org/en/ "Nginx"