'use strict';

/**
 * @ngdoc overview
 * @name tutor-timetable
 * @description
 * # Tutor Timetable Web Application
 *
 * Main module of the application.
 */
angular.module("tutor-timetable", [
    'ui.router',
    'base64',
    'ngAnimate',
    'ngMessages',
    'ngAria',
    'ngSanitize',
    'ngTouch',
    'ui.calendar',
    'isteven-multi-select',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'ngDraggable',
    'smart-table',
    'ngFileUpload',
    'ngFileSaver',
])

.config(['$stateProvider', '$urlRouterProvider', 'USER_ROLES', function ($stateProvider, $urlRouterProvider, USER_ROLES) {

    $urlRouterProvider.otherwise('/account');

    $stateProvider
        .state('page', {
            abstract: true,
            templateUrl: 'view/root.html'
        })
        .state('page.login', {
            url: '/login',
            templateUrl: 'login.html',
            controller: 'LoginController',
            access: {
                loggedIn: false
            }
        })
        .state('page.register', {
            url: '/register',
            templateUrl: 'view/register.html',
            controller: 'RegisterController',
            access: {
                loggedIn: false
            }
        })
        .state('page.account', {
            url: '/account',
            templateUrl: 'view/account.html',
            controller: 'AccountController',
            access: {
                loggedIn: true,
            }
        })
        .state('page.tutorapplication', {
            url: '/tutor/application',
            templateUrl: 'view/tutor-application.html',
            controller: 'TutorApplicationController',
            resolve: {
                TutorService: "TutorService",
                tutorPromise: function (TutorService) {
                    return TutorService.loadCurrentTutor();
                },
            },
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.TUTOR],
            }
        })
        .state('page.tutortimetable', {
            url: '/tutor/timetable',
            templateUrl: 'view/tutor-timetable.html',
            controller: 'TutorTimetableController',
            resolve: {
                TutorialService: "TutorialService",
                tutorialPromise: function (TutorialService) {
                    return TutorialService.loadCurrentTutorAssignedTutorials();
                },
            },
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.TUTOR],
            }
        })
        .state('page.unitpriority', {
            url: '/unit/priority',
            templateUrl: 'view/unit-priority.html',
            controller: 'UnitPriorityController',
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.COORDINATOR],
            }
        })
        .state('page.adminunitpriority', {
            url: '/unit/priority/admin',
            templateUrl: 'view/unit-priority.html',
            controller: 'UnitPriorityController',
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.ADMIN],
            }
        })
        .state('page.unittimetable', {
            url: '/unit/timetable',
            templateUrl: 'view/unit-timetable.html',
            controller: 'UnitTimetableController',
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.COORDINATOR],
            }
        })
        .state('page.adminunittimetable', {
            url: '/unit/timetable/admin',
            templateUrl: 'view/unit-timetable.html',
            controller: 'UnitTimetableController',
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.ADMIN],
            }
        })
        .state('page.units', {
            url: '/units',
            templateUrl: 'view/unit-management.html',
            controller: 'UnitManagementController',
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.ADMIN],
            }
        })
        .state('page.users', {
            url: '/users',
            templateUrl: 'view/user-management.html',
            controller: 'UserManagementController',
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.ADMIN],
            }
        })
        .state('page.administration', {
            url: '/administration',
            templateUrl: 'view/administration.html',
            controller: 'AdministrationController',
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.ADMIN],
            }
        })
        .state('page.tutorials', {
            url: '/tutorials',
            templateUrl: 'view/tutorial-management.html',
            controller: 'TutorialManagementController',
            access: {
                loggedIn: true,
                permissions: [USER_ROLES.ADMIN],
            }
        });
}]);
