'use strict';

angular.module('tutor-timetable')

.service('TutorialGroupService', [
    '$http',
    'BACKEND',
    function ($http, BACKEND) {

        return {
            all: function () {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/tutorial/group"
                });
            },

            create: function (group) {
                return $http({
                    method: 'post',
                    url: BACKEND.LOCATION + "/tutorial/group",
                    data: group
                });

            },

            update: function (group) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutorial/group",
                    data: group
                });

            },

            find: function (groupId) {

                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/tutorial/group/" + groupId
                });
            },

            delete: function (groupId) {

                return $http({
                    method: 'delete',
                    url: BACKEND.LOCATION + "/tutorial/group/" + groupId
                });
            },
            loadTutorialGroupsForUnit: function (unitId) {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/tutorial/group/unit/" + unitId
                });
            },
            assignTutorialGroup: function (groupId, tutorId) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutorial/group/" + groupId + "/tutor/" + tutorId + "/assign",
                });
            },
            clearTutorialGroupAssignment: function (groupId) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutorial/group/" + groupId + "/tutor/unassign",
                });
            },
        };
    }
]);
