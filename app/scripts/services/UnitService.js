'use strict';

angular.module('tutor-timetable')

.service('UnitService', [
    '$http',
    'BACKEND',
    'AuthenticationService',
    'USER_ROLES',
    'Upload',
    function ($http, BACKEND, AuthenticationService, USER_ROLES, Upload) {

        return {
            all: function () {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/unit"
                });
            },

            create: function (unit) {
                return $http({
                    method: 'post',
                    url: BACKEND.LOCATION + "/unit",
                    data: unit
                });

            },

            update: function (unit) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/unit",
                    data: unit
                });

            },

            find: function (unitId) {

                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/unit/" + unitId
                });
            },

            delete: function (unitId) {

                return $http({
                    method: 'delete',
                    url: BACKEND.LOCATION + "/unit/" + unitId
                });
            },

            getUnitsForCurrentUser: function () {
                var curUser = AuthenticationService.getCurrentUser();
                if (curUser.roles.indexOf(USER_ROLES.ADMIN) !== -1) {
                    return this.loadAllUnits();
                } else {
                    return $http({
                        method: 'get',
                        url: BACKEND.LOCATION + "/unit/user/" + curUser.userId,
                    });
                }
            },

            loadAllUnits: function () {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/unit/load",
                });
            },

            assignUnitCoordinator: function (unitId, coordinatorId) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/unit/" + unitId + "/assign/" + coordinatorId,
                });
            },

            uploadCsv: function (file) {
                return Upload.upload({
                    url: BACKEND.LOCATION + "/upload/unitcsv",
                    data: {
                        file: file
                    }
                });
            },
        };
    }
]);
