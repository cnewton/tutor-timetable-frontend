'use strict';

angular.module('tutor-timetable')

.service('AuthorisationService', [
    function () {

        this.authorise = function (user, state) {
            var auth = false;
            if (state.access.permissions === undefined || state.access.permissions.length === 0) {
                auth = true;

                //If only one role is required, search for it.
            } else if (state.access.permissions.length === 1) {
                angular.forEach(user.roles, function (role) {
                    if (role === state.access.permissions[0]) {
                        auth = true;
                    }
                });

                //If multiple required roles, find if any are missing.
            } else {
                auth = true;
                angular.forEach(state.access.permissions, function (requiredPermission) {
                    angular.forEach(user.roles, function (role) {
                        if (role !== requiredPermission) {
                            auth = false;
                        }
                    });
                });
            }
            return auth;
        };
  }
]);
