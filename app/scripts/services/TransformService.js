'use strict';

angular.module('tutor-timetable')

.service('TransformService', [
    '$http',
    'BACKEND',
    'moment',
    function ($http, BACKEND, moment) {
        var self = this;
        var daysOfWeek = ["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"];

        /* Transforms java DayOfWeek / LocalTime pairs into javascript moment times */
        this.transformAvailabiltiesToJS = function (availabilities) {
            var jsAvailabilties = [];

            availabilities.some(function (avail) {
                var startDT = moment();
                var endDT = moment();
                var avbDay = daysOfWeek.indexOf(avail.day);
                if (avbDay) {
                    var dayDiff = avbDay - startDT.day();

                    var stTimeParts = avail.startTime.split(":");
                    startDT.date(startDT.date() + dayDiff);
                    startDT.hours(stTimeParts[0]);
                    startDT.minutes(stTimeParts[1]);
                    startDT.startOf('minute');

                    var enTimeParts = avail.endTime.split(":");
                    endDT.date(endDT.date() + dayDiff);
                    endDT.hours(enTimeParts[0]);
                    endDT.minutes(enTimeParts[1]);
                    endDT.startOf('minute');

                    var jsAvail = {
                        id: avail.id,
                        title: 'Available',
                        start: startDT,
                        end: endDT,
                    };
                    jsAvailabilties.push(jsAvail);
                }
            });
            return jsAvailabilties;
        };

        /* Transforms javascipt moment times into a java DayOfWeek / LocalTime pair */
        this.transformAvailabiltiesToJava = function (availabilities) {
            var javaAvailabilities = [];

            availabilities.some(function (avail) {
                var javaAvail = {
                    day: daysOfWeek[avail.start.day()],
                    startTime: avail.start.format("HH:mm:ss"),
                    endTime: avail.end.format("HH:mm:ss"),
                };
                javaAvailabilities.push(javaAvail);
            });

            return javaAvailabilities;
        };

        /* Applies transformations to basic data in the application for use in the UI */
        this.transformTutorApplicationFields = function (application) {
            //Create failed units array if it does not yet exist.
            application.failedUnits = application.failedUnits || [];

            //Switch to string representation for view matching
            application.year = application.year.toString();

            var preferredUnits = [];
            application.preferredUnits.some(function (prefUnit) {
                preferredUnits.push({
                    id: prefUnit.unit.id,
                    name: prefUnit.unit.unitName,
                });
            });
            application.preferredUnits = preferredUnits;

            return application;
        };

        /* Transforms java LocalDateTime into javascript moment times and adds fields from the tutorial group and unit to the tutorial.*/
        this.transformTutorialTimesToJS = function (tutorials) {
            var jsTutorials = [];

            tutorials.some(function (tutorial) {

                var startDT = moment(tutorial.startTime);
                var endDT = moment(tutorial.endTime);

                var jsEvent = {
                    id: tutorial.id,
                    title: tutorial.room,
                    start: startDT,
                    end: endDT,
                    unitName: tutorial.tutorialGroup.unit.unitName,
                    groupName: tutorial.tutorialGroup.groupName,
                };
                jsTutorials.push(jsEvent);
            });

            return jsTutorials;
        };

        this.transformTutorialDTOsToJS = function (tutorials) {
            var jsTutorials = [];

            tutorials.some(function (tutorial) {

                var startDT = moment(tutorial.startTime);
                var endDT = moment(tutorial.endTime);

                var jsEvent = {
                    id: tutorial.id,
                    tutorId: tutorial.tutorId,
                    title: tutorial.room,
                    start: startDT,
                    end: endDT,
                    stick: true,
                };
                jsTutorials.push(jsEvent);
            });

            return jsTutorials;
        };

        this.transformTimeToJSDate = function (time) {
            return moment(time).toDate();
        };

        this.transformTutorialGroupToJS = function (group) {
            group.tutorials.some(function (tute) {
                self.transformTutorialTimesToJSDate(tute);
            });
        };

        this.transformTutorialTimesToJSDate = function (tutorial) {
            tutorial.startTime = self.transformTimeToJSDate(tutorial.startTime);
            tutorial.endTime = self.transformTimeToJSDate(tutorial.endTime);
            return tutorial;
        };

        this.transformTimeToJava = function (time) {
            return moment(time).format("YYYY-MM-DDTHH:mm:ss");
        };

        this.transformSystemStateDatesToJS = function (state) {
            state.semesterStart = self.transformDateToJS(state.semesterStart);
            state.semesterEnd = self.transformDateToJS(state.semesterEnd);
            state.applicationStart = self.transformDateToJS(state.applicationStart);
            state.applicationEnd = self.transformDateToJS(state.applicationEnd);
            state.lockout = self.transformDateToJS(state.lockout);
        };

        this.transformDateToJS = function (date) {
            return moment(date).toDate();
        };

        this.transformDateToJava = function (date) {
            var momentDate = moment(date).format("YYYY-MM-DD");
            // Backend requires surrounding quotes for single non-json body.
            return '"' + momentDate + '"';
        };
    }
]);
