'use strict';

angular.module('tutor-timetable')

.service('TutorService', [
    '$http',
    'BACKEND',
    'AuthenticationService',
    function ($http, BACKEND, AuthenticationService) {

        return {
            register: function (tutor) {
                return $http({
                    method: 'post',
                    url: BACKEND.LOCATION + "/user/register/tutor",
                    data: tutor
                });
            },

            addAvailabilityList: function (tutorId, availabilities) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutor/" + tutorId + "/availability/add",
                    data: availabilities,
                });
            },

            removeAvailabilityList: function (tutorId, availabilities) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutor/" + tutorId + "/availability/subtract",
                    data: availabilities,
                });
            },

            updateTutorDetails: function (tutorId, details) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutor/" + tutorId + "/details",
                    data: details,
                });
            },

            loadCurrentTutor: function () {
                var currentUser = AuthenticationService.getCurrentUser();
                return this.loadTutor(currentUser.userId);
            },

            loadTutor: function (tutorId) {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/tutor/" + tutorId,
                });
            },
        };
    }
]);
