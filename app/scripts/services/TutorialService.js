'use strict';

angular.module('tutor-timetable')

.service('TutorialService', [
    '$http',
    'BACKEND',
    'AuthenticationService',
    'Upload',
    function ($http, BACKEND, AuthenticationService, Upload) {

        return {
            all: function () {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/tutorial"
                });
            },

            create: function (tutorial) {
                return $http({
                    method: 'post',
                    url: BACKEND.LOCATION + "/tutorial",
                    data: tutorial
                });

            },

            update: function (tutorial) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutorial",
                    data: tutorial
                });

            },

            find: function (tutorialId) {

                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/tutorial/" + tutorialId
                });
            },

            delete: function (tutorialId) {

                return $http({
                    method: 'delete',
                    url: BACKEND.LOCATION + "/tutorial/" + tutorialId
                });
            },

            loadAssignedTutorials: function (tutorId) {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/tutorial/tutor/" + tutorId,
                });
            },

            loadCurrentTutorAssignedTutorials: function () {
                var currentUser = AuthenticationService.getCurrentUser();
                return this.loadAssignedTutorials(currentUser.userId);
            },
            uploadCsv: function (file) {
                return Upload.upload({
                    url: BACKEND.LOCATION + "/upload/tutorialcsv",
                    data: {
                        file: file
                    }
                });
            },
            assignTutorial: function (tutorialId, tutorId) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutorial/" + tutorialId + "/tutor/" + tutorId + "/assign",
                });
            },
            clearTutorialAssignment: function (tutorialId) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutorial/" + tutorialId + "/tutor/unassign",
                });
            },
        };
    }
]);
