'use strict';

angular.module('tutor-timetable')

.service('TutorPreferenceService', [
    '$http',
    'BACKEND',
    function ($http, BACKEND) {

        return {
            addUnitPreference: function (tutorId, unitId) {
                return $http({
                    method: 'post',
                    url: BACKEND.LOCATION + "/tutor/preference/create",
                    params: {
                        tutorId: tutorId,
                        unitId: unitId,
                    },
                });
            },

            updateUnitPreferencePriority: function (prefId, priority) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutor/preference/" + prefId + "/priority",
                    params: {
                        priority: priority,
                    },
                });
            },

            addUnitPreferenceList: function (tutorId, unitIdList) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutor/" + tutorId + "/preference/add",
                    data: unitIdList,
                });
            },

            removeUnitPreferenceList: function (tutorId, unitIdList) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/tutor/" + tutorId + "/preference/subtract",
                    data: unitIdList,
                });
            },

            getTutorPreferencesForUnit: function (unitId) {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/unit/" + unitId + "/preference",
                });
            },

            deleteTutorPreference: function (preferenceId) {
                return this.updateUnitPreferencePriority(preferenceId, -1);
            },
        };
    }
]);
