'use strict';

angular.module('tutor-timetable')

.service('AdministrationService', [
    '$http',
    'BACKEND',
    '$window',
    'FileSaver',
    'Blob',
    'moment',
    function ($http, BACKEND, $window, FileSaver, Blob, moment) {

        return {
            getSystemState: function () {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/admin/systemstate"
                });
            },

            setTutorsMaxHours: function (hours) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/systemstate/tutor/hours",
                    params: {
                        maxHours: hours
                    },
                });
            },

            setSemesterStartDate: function (date) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/systemstate/semester/start",
                    data: date
                });
            },

            setSemesterEndDate: function (date) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/systemstate/semester/end",
                    data: date
                });
            },

            setApplicationStartDate: function (date) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/systemstate/application/start",
                    data: date
                });
            },

            setApplicationEndDate: function (date) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/systemstate/application/end",
                    data: date
                });
            },

            setLockoutDate: function (date) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/systemstate/lockout",
                    data: date
                });
            },

            toggleTutorAccounts: function () {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/tutors/lock/toggle"
                });
            },

            toggleCoordinatorAccounts: function () {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/coordinators/lock/toggle"
                });
            },

            deleteAllUnits: function () {
                return $http({
                    method: 'delete',
                    url: BACKEND.LOCATION + "/admin/units/wipe",
                });
            },

            deleteAllTutorials: function () {
                return $http({
                    method: 'delete',
                    url: BACKEND.LOCATION + "/admin/tutorials/wipe",
                });
            },

            deleteAllTutorAssignments: function () {
                return $http({
                    method: 'delete',
                    url: BACKEND.LOCATION + "/admin/assignments/wipe",
                });
            },

            deleteAllTutorApplications: function () {
                return $http({
                    method: 'delete',
                    url: BACKEND.LOCATION + "/admin/applications/wipe",
                });
            },

            //This version of the download functionality does not work in Internet Explorer / EDGE
            downloadUnitCsv: function () {
                $http({
                    method: 'get',
                    url: BACKEND.LOCATION + '/download/unitcsv',
                }).then(function successCallback(response) {
                    var blob = new Blob([response.data], {
                        type: 'text/csv',
                    });
                    FileSaver.saveAs(blob, "units" + moment().format("DDMMM") + ".csv");
                });
            },

            //This version of the download functionality does not work in Internet Explorer / EDGE
            downloadTutorialCsv: function () {
                $http({
                    method: 'get',
                    url: BACKEND.LOCATION + '/download/tutorialcsv',
                }).then(function successCallback(response) {
                    var blob = new Blob([response.data], {
                        type: 'text/csv',
                    });
                    FileSaver.saveAs(blob, "tutorials" + moment().format("DDMMM") + ".csv");
                });
            },

            assignTutors: function () {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/tutorials/assign",
                });
            },

            setTutorMaxHours: function (hours) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/admin/systemstate/tutor/hours",
                    params: {
                        maxHours: hours
                    }
                });
            },
        };
    }
]);
