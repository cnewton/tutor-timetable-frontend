'use strict';

angular.module('tutor-timetable')

.service('UserService', [
    '$http',
    'BACKEND',
    function ($http, BACKEND) {

        return {
            all: function () {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/user",
                });
            },

            create: function (user) {
                return $http({
                    method: 'post',
                    url: BACKEND.LOCATION + "/user",
                    data: user
                });

            },

            update: function (user) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/user",
                    data: user,
                });
            },

            find: function (userId) {

                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/user/" + userId,
                });
            },

            delete: function (userId) {

                return $http({
                    method: 'delete',
                    url: BACKEND.LOCATION + "/user/" + userId,
                });
            },

            account: function () {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/account",
                });
            },

            adminUpdatePassword: function (userId, password) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/user/" + userId + "/password/force",
                    data: '"'+password+'"',
                });
            },

            updatePassword: function (userId, oldPassword, newPassword) {
                return $http({
                    method: 'put',
                    url: BACKEND.LOCATION + "/user/" + userId + "/password",
                    data: [oldPassword, newPassword],
                });
            },

            getAllUnitCoordinators: function () {
                return $http({
                    method: 'get',
                    url: BACKEND.LOCATION + "/unitcoordinator",
                });
            },
        };
    }
]);
