'use strict';

angular.module('tutor-timetable')

.service('TutorApplicationService', [
    function () {

        /* Creates array of units in the format expected by isteven-multi-select.
        Marks units that are also found in the preferredUnits list as selected. */
        this.getUnitSelectionList = function (units, preferredUnits) {
            var unitSelectionList = [];
            units.some(function (unit) {
                var viewUnit = {
                    isSelected: false,
                    id: unit.id,
                    name: unit.unitName,
                };
                preferredUnits.some(function (prefUnit) {
                    if (prefUnit.id === unit.id) {
                        viewUnit.isSelected = true;
                        return true;
                    }
                });
                unitSelectionList.push(viewUnit);
            });
            return unitSelectionList;
        };

        /* Gets the delta additions from originalUnits -> selectedUnits */
        this.getPreferredUnitAdditions = function (selectedUnits, originalUnits) {
            var additions = [];
            selectedUnits.some(function (selectedUnit) {
                additions.push(selectedUnit.id);
                originalUnits.some(function (originalUnit) {
                    if (selectedUnit.id === originalUnit.id) {
                        additions.splice(additions.indexOf(selectedUnit.id), 1);
                    }
                });
            });

            return additions;
        };

        /* Gets the delta subtractions from originalUnits -> selectedUnits */
        this.getPreferredUnitSubtractions = function (selectedUnits, originalUnits) {
            var subtractions = [];
            originalUnits.some(function (originalUnit) {
                subtractions.push(originalUnit.id);
                selectedUnits.some(function (selectedUnit) {
                    if (selectedUnit.id === originalUnit.id) {
                        subtractions.splice(subtractions.indexOf(originalUnit.id), 1);
                    }
                });
            });

            return subtractions;
        };

        /* Gets the delta additions from originalEvents -> events */
        this.getAvailabilityAdditions = function (events, originalEvents) {
            var additions = [];
            events.some(function (event) {
                additions.push(event);
                originalEvents.some(function (originalEvent) {
                    if (event.id === originalEvent.id) {
                        additions.splice(additions.indexOf(event), 1);
                    }
                });
            });

            return additions;
        };

        /* Gets the delta subtractions from originalEvents -> events */
        this.getAvailabilitySubtractions = function (events, originalEvents) {

            var subtractions = [];
            originalEvents.some(function (originalEvent) {
                subtractions.push(originalEvent.id);
                events.some(function (event) {
                    if (event.id === originalEvent.id) {
                        subtractions.splice(subtractions.indexOf(originalEvent.id), 1);
                    }
                });
            });

            return subtractions;

        };

        this.getApplicationDetails = function (application) {
            return {
                course: application.course,
                year: application.year,
                failedUnits: application.failedUnits,
            };
        };
}]);
