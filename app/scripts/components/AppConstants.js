'use strict';

angular.module("tutor-timetable")

.constant('USER_ROLES', Object.freeze({
    ADMIN: 'ROLE_ADMIN',
    COORDINATOR: 'ROLE_COORDINATOR',
    TUTOR: 'ROLE_TUTOR',
    USER: 'ROLE_USER',
}))

.constant("moment", moment)

.value('BACKEND', {
    LOCATION: '',
    AUTH_HEADER: 'X-AUTH-TOKEN',
})

// Set the backend location from the current host.
.run(['$location', 'BACKEND', function ($location, BACKEND) {
    BACKEND.LOCATION = 'http://' + $location.host() + ':8080/tutor-timetable';
}]);
