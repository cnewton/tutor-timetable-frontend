'use strict';

angular.module("tutor-timetable")

.value('COLORS', {
    calendar: {
        val: "#3a87ad",
        style: {
            'background-color': "#3a87ad",
        },
    },
    unitCoordinator: {
        val: "#000000",
        style: {
            'background-color': "#000000",
        },
    },
    list: [
        {
            val: "#AA22FF",
            style: {
                'background-color': "#AA22FF",
            },
        },
        {
            val: "#AAAA22",
            style: {
                'background-color': "#AAAA22",
            },
        },
        {
            val: "#FF8888",
            style: {
                'background-color': "#FF8888",
            },
        },
        {
            val: "#AA8FF8",
            style: {
                'background-color': "#AA8FF8",
            },
        },
        {
            val: "#AAFFAA",
            style: {
                'background-color': "#AAFFAA",
            },
        },
        {
            val: "#46F6AF",
            style: {
                'background-color': "#46F6AF",
            },
        },
        {
            val: "#30FF30",
            style: {
                'background-color': "#30FF30",
            },
        },
        {
            val: "#777155",
            style: {
                'background-color': "#777155",
            },
        },
        {
            val: "#AA5555",
            style: {
                'background-color': "#AA5555",
            },
        },
        {
            val: "#FAAC15",
            style: {
                'background-color': "#FAAC15",
            },
        },
    ]
});
