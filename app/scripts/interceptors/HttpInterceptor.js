'use strict';

angular.module('tutor-timetable')

.factory('httpRequestInterceptor', [
    'BACKEND',
    '$window',
    '$q',
    function (BACKEND, $window, $q) {
        return {
            request: function (config) {
                config.headers[BACKEND.AUTH_HEADER] = $window.sessionStorage.getItem(BACKEND.AUTH_HEADER);
                return config;
            },
            requestError: function (error) {
                return error;
            },
            response: function (response) {
                return response;
            },
            responseError: function (error) {
                if (error.status === -1) {
                    $window.location.assign('/#/login');
                }
                return $q.reject(error);
            }
        };
    }
])

.config([
    '$httpProvider',
    function ($httpProvider) {
        $httpProvider.interceptors.push('httpRequestInterceptor');
}]);
