'use strict';


angular.module('tutor-timetable').run([
  '$rootScope',
  '$state',
  'AuthorisationService',
  'AuthenticationService',
  function ($rootScope, $state, AuthorisationService, AuthenticationService) {

        /**
         * Whenever the page changes, check to make sure the user is logged in, if they are not,
         * return them to the login page. If they are logged in but not authorized, return them
         * to the accounts page. Otherwise proceed.
         */
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            var authorised, currentUser;

            if (toState.access !== undefined && toState.access.loggedIn === true) {

                currentUser = AuthenticationService.getCurrentUser();

                if (currentUser === null || currentUser === undefined) {
                    event.preventDefault();
                    $state.go('page.login');
                } else {
                    authorised = AuthorisationService.authorise(currentUser, toState);

                    if (!authorised) {
                        event.preventDefault();
                        $state.go('page.account');
                    }
                }

            }
        });
    }
]);
