'use strict';

angular.module('tutor-timetable')

.controller('RegisterController', [
    '$scope',
    'TutorService',
    '$state',
    '$uibModal',
    function ($scope, TutorService, $state, $uibModal) {

        $scope.registerState = {
            hasError: false,
            error: "",
        };

        //Local Helper functions
        var openModel = function (message) {
            return $uibModal.open({
                template: message,
            });
        };

        //Form submission handlder
        $scope.processForm = function (registration) {
            if ($scope.registerForm.$valid) {
                if (registration.password !== registration.repeatPassword) {
                    $scope.registerState.hasError = true;
                    $scope.registerState.error = "Passwords do not match";
                } else {
                    var tutor = {
                        "@class": "Tutor",
                        name: registration.name,
                        email: registration.email,
                        mobile: registration.mobile,
                        staffId: registration.staffId,
                        studentId: registration.studentId,
                        username: registration.username,
                        password: registration.password,
                    };
                    var modal = openModel("Please wait...");
                    TutorService.register(tutor).then(
                        function successCallback() {
                            modal.dismiss();
                            openModel("Success, please log in.");
                            $state.go('page.login');
                        },
                        function errorCallback(response) {
                            modal.dismiss();
                            $scope.registerState.hasError = true;
                            $scope.registerState.error = response.data;
                        }
                    );
                }
            }
        };
    }
]);
