'use strict';

angular.module("tutor-timetable")

.controller('NavbarController', [
    '$scope',
    '$state',
    '$rootScope',
    'AuthenticationService',
    'USER_ROLES',
    function ($scope, $state, $rootScope, AuthenticationService, USER_ROLES) {

        $scope.currentStateName = $state.current.name;

        $scope.navBarItems = [
            {
                sref: 'page.unittimetable',
                title: 'Unit Timetables',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    return (curUser && curUser.roles.indexOf(USER_ROLES.COORDINATOR) !== -1);
                }
            },
            {
                sref: 'page.adminunittimetable',
                title: 'Unit Timetables',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    return (curUser && curUser.roles.indexOf(USER_ROLES.ADMIN) !== -1);
                }
            },
            {
                sref: 'page.unitpriority',
                title: 'Unit Priorities',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    return (curUser && curUser.roles.indexOf(USER_ROLES.COORDINATOR) !== -1);
                }
            },
            {
                sref: 'page.adminunitpriority',
                title: 'Unit Priorities',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    return (curUser && curUser.roles.indexOf(USER_ROLES.ADMIN) !== -1);
                }
            },
            {
                sref: 'page.tutortimetable',
                title: 'Timetable',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    return (curUser && curUser.roles.indexOf(USER_ROLES.TUTOR) !== -1);
                }
            },
            {
                sref: 'page.tutorapplication',
                title: 'Application',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    //Viewable only to Tutors who are not also coordinators.
                    return (curUser && curUser.roles.indexOf(USER_ROLES.TUTOR) !== -1 &&
                        curUser.roles.indexOf(USER_ROLES.COORDINATOR) === -1);
                }
            },
            {
                sref: 'page.administration',
                title: 'Administration',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    return (curUser && curUser.roles.indexOf(USER_ROLES.ADMIN) !== -1);
                },
            },
            {
                sref: 'page.account',
                title: 'Account',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    if (curUser) {
                        return true;
                    }
                    return false;
                },
            },
        ];

        $scope.navBarGroup1 = [
            {
                sref: 'page.units',
                title: 'Units',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    return (curUser && curUser.roles.indexOf(USER_ROLES.ADMIN) !== -1);
                }
            },
            {
                sref: 'page.tutorials',
                title: 'Tutorials',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    return (curUser && curUser.roles.indexOf(USER_ROLES.ADMIN) !== -1);
                }
            },
            {
                sref: 'page.users',
                title: 'Users',
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    return (curUser && curUser.roles.indexOf(USER_ROLES.ADMIN) !== -1);
                }
            },
        ];
        $scope.navBarGroup1.viewable = function () {
            var curUser = AuthenticationService.getCurrentUser();
            return (curUser && curUser.roles.indexOf(USER_ROLES.ADMIN) !== -1);
        };

        $scope.navBarTasks = [
            {
                style: 'cursor: pointer;',
                title: 'Logout',
                clicked: function () {
                    AuthenticationService.logout();
                    $state.go("page.login");
                },
                viewable: function () {
                    var curUser = AuthenticationService.getCurrentUser();
                    if (curUser) {
                        return true;
                    }
                    return false;
                }
            }
        ];


        // When the page changes, update the current state name
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            $scope.currentStateName = toState.name;
        });
    }
]);
