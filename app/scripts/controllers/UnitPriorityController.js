'use strict';

angular.module('tutor-timetable')

.controller('UnitPriorityController', [
    '$scope',
    'uiCalendarConfig',
    'UnitService',
    '$filter',
    'TutorPreferenceService',
    '$rootScope',
    function ($scope, uiCalendarConfig, UnitService, $filter, TutorPreferenceService, $rootScope) {

        $scope.selectedUnit = {};
        $scope.units = [];
        $scope.tutors = [];
        $scope.expanded = [];

        var isDragging = false;

        $rootScope.$on('draggable:start', function () {
            isDragging = true;
        });

        /*Local helper functions */
        //Sorts tutor preferences from lowest to highest, moves 0 (unassigned) to end.
        var sortTutorPreferences = function () {
            $scope.tutors = $filter("orderBy")($scope.tutors, "priority");
            var unassigned = [];
            var declined = [];
            var assigned = [];
            $scope.tutors.some(function (obj) {
                if (obj.priority === 0) {
                    unassigned.push(obj);
                } else if (obj.priority === -1) {
                    declined.push(obj);
                } else {
                    assigned.push(obj);
                }
            });
            $scope.tutors = assigned.concat(unassigned.concat(declined));
        };

        /*Event handlers*/
        $scope.selectUnit = function (unit) {
            if ($scope.units.indexOf(unit) !== -1) {
                $scope.selectedUnit = unit;
                TutorPreferenceService.getTutorPreferencesForUnit(unit.id).then(
                    function successCallback(response) {
                        $scope.tutors = response.data;
                        sortTutorPreferences();
                    }
                );
            }
        };

        $scope.updatePriority = function (tutor, oldVal) {
            //If the priority is not valid, return it to its old value.
            if (!tutor.priority && tutor.priority !== 0 || tutor.priority < 0 || tutor.priority > 20) {
                tutor.priority = oldVal;
            } else {
                TutorPreferenceService.updateUnitPreferencePriority(tutor.preferenceId, tutor.priority);
                sortTutorPreferences();
            }
        };

        $scope.dropComplete = function (idx, dragItem) {

            var dropItem = $scope.tutors[idx];
            var dropItemPriority = dropItem.priority;
            var dragItemPriority = dragItem.priority;

            dragItem.priority = dropItem.priority;
            dropItem.priority = dragItemPriority;

            $scope.updatePriority(dropItem, dropItemPriority);
            $scope.updatePriority(dragItem, dragItemPriority);
        };

        $scope.deleteDrop = function (data) {
            TutorPreferenceService.deleteTutorPreference(data.preferenceId);
            data.priority = -1;
            sortTutorPreferences();
        };

        $scope.expand = function (tutor) {
            if (isDragging) {
                isDragging = false;
            } else {
                if ($scope.expanded.indexOf(tutor) === -1) {
                    $scope.expanded.push(tutor);
                } else {
                    $scope.expanded.splice($scope.expanded.indexOf(tutor), 1);
                }
            }
        };

        //Population
        UnitService.getUnitsForCurrentUser().then(
            function successCallback(response) {
                $scope.units = response.data;
                if ($scope.units.length > 0) {
                    $scope.selectUnit($scope.units[0]);
                }
            }
        );
    }
]);
