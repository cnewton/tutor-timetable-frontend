'use strict';

angular.module("tutor-timetable")

.controller("UnitManagementController", [
    '$scope',
    'UnitService',
    '$timeout',
    '$window',
    'UserService',
    function ($scope, UnitService, $timeout, $window, UserService) {


        UserService.getAllUnitCoordinators().then(
            function successCallback(response) {
                $scope.coordinators = response.data;
            }
        );

        /*Local helper functions*/

        function populate() {
            UnitService.loadAllUnits().then(
                function successCallback(response) {
                    $scope.units = response.data;
                }
            );
        }

        populate();

        /*Event handlers*/

        $scope.updateUnit = function (unit) {
            UnitService.update(unit).then(
                function successCallback(response) {
                    var arrayPos = $scope.units.indexOf(unit);
                    $scope.units[arrayPos] = response.data;
                }
            );
        };

        $scope.newUnit = function () {

            var unit = {
                unitName: "AAA New Unit",
                unitCode: "AAA Code",
            };
            UnitService.create(unit).then(
                function successCallback(response) {
                    $scope.units.push(response.data);
                }
            );
        };

        $scope.setCoordinator = function (unitId, coordinatorId) {
            UnitService.assignUnitCoordinator(unitId, coordinatorId);
        };

        $scope.deleteUnit = function (unit) {
            if ($window.confirm("Are you sure you want to delete this?")) {
                var arrayPos = $scope.units.indexOf(unit);
                $scope.units.splice(arrayPos, 1);
                UnitService.delete(unit.id);
            }
        };

        $scope.uploadUnitCSV = function (file) {
            if (file) {
                $scope.upload = file;
                UnitService.uploadCsv(file).then(
                    function successCallback() {
                        populate();
                        $scope.upload = null;
                    },
                    function errorCallback(response) {
                        file.result = response.data;
                    },
                    function notifyCallback(response) {
                        file.progress = Math.min(100, parseInt(100.0 * response.loaded / response.total));
                    }
                );
            }
        };


    }
]);
