'use strict';

angular.module('tutor-timetable')

.controller('AccountController', [
    '$scope',
    'UserService',
    'AuthenticationService',
    'USER_ROLES',
    function ($scope, UserService, AuthenticationService, USER_ROLES) {

        $scope.userDetailsState = {
            hasError: false,
            error: "",
        };

        var currUser = AuthenticationService.getCurrentUser();

        if (currUser.roles.indexOf(USER_ROLES.ADMIN) !== -1) {
            $scope.accountType = "ADMIN";
        } else if (currUser.roles.indexOf(USER_ROLES.COORDINATOR) !== -1) {
            $scope.accountType = "COORDINATOR";
        } else if (currUser.roles.indexOf(USER_ROLES.TUTOR) !== -1) {
            $scope.accountType = "TUTOR";
        }

        UserService.account().then(
            function successCallback(response) {
                $scope.user = response.data;
            }
        );

        //Form submission handlder
        $scope.processForm = function (form) {
            $scope.userDetailsState.hasError = false;
            var user = $scope.user;
            if (form.$valid) {

                //Password update handling
                if (user.password) {
                    if (user.password !== user.repeatPassword) {
                        $scope.userDetailsState.hasError = true;
                        $scope.userDetailsState.error = "New passwords do not match";
                    } else {
                        if ($scope.accountType === 'ADMIN') {
                            UserService.adminUpdatePassword(user.id, user.password).then(
                                function successCallback() {},
                                function errorCallback(response) {
                                    $scope.userDetailsState.hasError = true;
                                    $scope.userDetailsState.error = response.data;
                                }
                            );
                        } else {
                            UserService.updatePassword(user.id, user.oldPassword, user.password).then(
                                function successCallback() {},
                                function errorCallback(response) {
                                    $scope.userDetailsState.hasError = true;
                                    $scope.userDetailsState.error = response.data;
                                }
                            );
                        }
                    }
                }

                //Details update handling
                UserService.update(user).then(
                    function successCallback(response) {
                        $scope.user = response.data;
                    },
                    function errorCallback(response) {
                        $scope.userDetailsState.hasError = true;
                        $scope.userDetailsState.error = response.data;
                    }
                );
            }
        };
    }
]);
