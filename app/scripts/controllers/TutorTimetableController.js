'use strict';

angular.module('tutor-timetable')

.controller('TutorTimetableController', [
    '$scope',
    'uiCalendarConfig',
    'tutorialPromise',
    'TransformService',
    function ($scope, uiCalendarConfig, tutorialPromise, TransformService) {

        $scope.tutorials = tutorialPromise.data;
        $scope.events = TransformService.transformTutorialTimesToJS($scope.tutorials);
        $scope.calendarSources = [$scope.events];

        var eventHandlers = {
            /* On Selection Completion */
            eventRender: function (event, element) {
                element.tooltip({
                    title: event.unitName + " " + event.groupName,
                });
            }
        };

        /* Calendar setup options */
        $scope.calendarOptions = {
            defaultView: 'agendaWeek',
            height: 550,
            allDaySlot: false,
            selectable: false,
            editable: false,
            selectHelper: false,
            selectOverlap: false,
            eventOverlap: false,
            hiddenDays: [0, 6],
            minTime: '06:30',
            maxTime: '21:00',
            columnFormat: 'ddd',
            eventConstraint: 'businessHours',
            timezone: 'local',
            businessHours: {
                start: '07:00',
                end: '20:30',
            },
            eventRender: eventHandlers.eventRender,
            header: {
                left: 'title',
                center: 'agendaWeek month',
                right: 'today prev,next',
            },
        };
    }
]);
