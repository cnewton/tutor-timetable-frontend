'use strict';

angular.module('tutor-timetable')

.controller('LoginController', [
    '$scope',
    '$state',
    'AuthenticationService',
    function ($scope, $state, AuthenticationService) {

        $scope.loginState = {
            failed: false,
            error: ''
        };

        $scope.login = function () {
            if ($scope.username && $scope.password) {
                AuthenticationService.login($scope.username, $scope.password).then(
                    function (loginState) {
                        $scope.loginState = loginState;
                        if (!$scope.loginState.failed) {
                            $state.go('page.account');
                        }
                    }
                );
            } else {
                $scope.loginState.failed = true;
                $scope.loginState.error = 'Please enter a username and password';
            }
        };
    }
]);
