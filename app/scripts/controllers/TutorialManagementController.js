'use strict';

angular.module("tutor-timetable")

.controller("TutorialManagementController", [
    '$scope',
    '$state',
    'TutorialService',
    'TutorialGroupService',
    'UnitService',
    'uiDatetimePickerConfig',
    '$window',
    'TransformService',
    function ($scope, $state, TutorialService, TutorialGroupService, UnitService, uiDatetimePickerConfig, $window, TransformService) {

        //For use in typeahead as well as for creating new groups.
        $scope.tutorialTypes = ["Laboratory", "Lecture", "Tutorial", "Workshop", "Computer Laboratory", "Seminar", "Practical", "Demonstration"];

        //Config required to have the datetimepicker appear next to the clicked cell.
        uiDatetimePickerConfig.appendToBody = true;

        //Initial population of units.
        UnitService.all().then(
            function successCallback(response) {
                $scope.units = response.data;
                if ($scope.units) {
                    $scope.selectUnit($scope.units[0]);
                }
            }
        );

        //Selects a unit, loads the tutorial groups for that unit.
        $scope.selectUnit = function (unit) {
            if (unit) {
                $scope.selectedUnit = unit;
                TutorialGroupService.loadTutorialGroupsForUnit(unit.id).then(
                    function successCallback(response) {
                        $scope.groups = response.data;
                        if ($scope.groups.length > 0) {
                            $scope.selectGroup($scope.groups[0]);
                        } else {
                            $scope.selectedGroup = {
                                groupName: "No Groups"
                            };
                        }
                        $scope.groups.some(function (grp) {
                            TransformService.transformTutorialGroupToJS(grp);
                        });
                    }
                );
            }
        };

        //Sets the selected group (dictates which tutorials are displayed)
        $scope.selectGroup = function (group) {
            if (group) {
                $scope.selectedGroup = group;
            }
        };

        $scope.newTutorial = function () {
            var group = $scope.selectedGroup;
            if (group.id) {
                var tutorial = {
                    room: "1 New",
                    startTime: TransformService.transformTimeToJava(new Date(1)),
                    endTime: TransformService.transformTimeToJava(new Date(1)),
                    tutorialGroup: $scope.selectedGroup,
                };

                TutorialService.create(tutorial).then(
                    function successCallback(response) {
                        group.tutorials.unshift(TransformService.transformTutorialTimesToJSDate(response.data));
                    }
                );
            } else {
                $window.confirm("Create a group first");
            }
        };

        $scope.newGroup = function () {
            var unit = $scope.selectedUnit;
            if ($scope.selectedUnit.id) {
                var group = {
                    unit: $scope.selectedUnit,
                    groupName: "New Group",
                    tutorialType: $scope.tutorialTypes[0],
                    tutorials: [],
                };
                TutorialGroupService.create(group).then(
                    function successCallback(response) {
                        if (unit === $scope.selectedUnit) {
                            $scope.groups.push(response.data);
                            $scope.selectGroup(response.data);
                            //Setup the tutorial array.
                            $scope.selectedGroup.tutorials = [];
                        }
                    }
                );
            }
        };

        $scope.updateTutorial = function (tutorial) {
            var tute = {};
            angular.copy(tutorial, tute);
            if (tute) {
                tute.startTime = TransformService.transformTimeToJava(tute.startTime);
                tute.endTime = TransformService.transformTimeToJava(tute.endTime);
                tute.tutorialGroup = $scope.selectedGroup;
                TutorialService.update(tute);
            }
        };

        $scope.updateGroup = function () {
            var group = {};
            angular.copy($scope.selectedGroup, group);
            if (group) {
                //Remove unnecessary data
                group.tutorials = null;
                group.unit = $scope.selectedUnit;
                TutorialGroupService.update(group);
            }
        };

        $scope.deleteTutorial = function (tutorial) {
            if ($window.confirm("Are you sure you want to delete this?")) {
                TutorialService.delete(tutorial.id);
                $scope.selectedGroup.tutorials.splice($scope.selectedGroup.tutorials.indexOf(tutorial), 1);
            }
        };

        $scope.deleteGroup = function () {
            if ($scope.selectedGroup.id) {
                if ($window.confirm("Are you sure you want to delete this? (This will delete all the tutorials in this group!)")) {
                    TutorialGroupService.delete($scope.selectedGroup.id);
                    $scope.groups.splice($scope.groups.indexOf($scope.selectedGroup), 1);
                    if ($scope.groups.length > 0) {
                        $scope.selectGroup($scope.groups[0]);
                    } else {
                        $scope.selectedGroup = {
                            groupName: "No Groups"
                        };
                    }
                }
            }
        };

        $scope.uploadTutorialCSV = function (file) {
            if (file) {
                $scope.upload = file;
                TutorialService.uploadCsv(file).then(
                    function successCallback() {
                        $state.reload();
                        $scope.upload = null;
                    },
                    function errorCallback(response) {
                        file.result = response.data;
                    },
                    function notifyCallback(response) {
                        file.progress = Math.min(100, parseInt(100.0 * response.loaded / response.total));
                    }
                );
            }
        };
}]);
