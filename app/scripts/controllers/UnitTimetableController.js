'use strict';

angular.module('tutor-timetable')

.controller('UnitTimetableController', [
    '$scope',
    '$q',
    '$timeout',
    '$compile',
    'uiCalendarConfig',
    'TutorialGroupService',
    'TutorialService',
    'UnitService',
    'TransformService',
    'TutorPreferenceService',
    'COLORS',
    '$rootScope',
    function ($scope, $q, $timeout, $compile, uiCalendarConfig, TutorialGroupService, TutorialService, UnitService, TransformService, TutorPreferenceService, COLORS, $rootScope) {

        $scope.tutors = [];
        $scope.selectedTutor = {};
        $scope.units = [];
        $scope.selectedUnit = {};
        $scope.groups = [];
        $scope.selectedGroup = {};

        $scope.events = [];
        $scope.calendarSources = [$scope.events];

        var promises = [];
        var isDragging = false;

        //Initial population of units.
        UnitService.getUnitsForCurrentUser().then(
            function successCallback(response) {
                $scope.units = response.data;
                if ($scope.units) {
                    $scope.selectUnit($scope.units[0]);
                }
            }
        );

        $rootScope.$on('draggable:start', function () {
            isDragging = true;
        });

        /* Local Helper Functions */
        var findTutorColor = function (tutorId) {
            var color;
            $scope.tutors.some(function (tutor) {
                if (tutor.id === tutorId) {
                    color = tutor.color;
                    return true;
                }
            });
            if (!color) {
                color = COLORS.calendar;
            }
            return color.val;
        };

        var recolorEvent = function (event, color) {
            var evts = uiCalendarConfig.calendars.calendar.fullCalendar('clientEvents', [event._id]);
            evts[0].color = color;
            uiCalendarConfig.calendars.calendar.fullCalendar('updateEvent', evts[0]);
        };

        var findEventByTutorialId = function (tutorialId) {
            var evt;
            $scope.events.some(function (event) {
                if (event.id === tutorialId) {
                    evt = event;
                    return true;
                }
            });
            return evt;
        };

        var colorEvents = function () {
            $scope.events.some(function (event) {
                var color = findTutorColor(event.tutorId);
                recolorEvent(event, color);
            });

        };


        /* Calendar Event handlers */
        var eventHandlers = {
            //This function will attach the drop zone attributes and properties required for each calendar event
            eventRender: function (event, element) {
                element.attr("ng-drop", "true");
                element.attr('ng-drop-success', "assignTutorial($data, " + event.id + ")");
                element.addClass('calendarDropEnlarge');
                $compile(element)($scope);
            }
        };

        /* Calendar setup options */
        $scope.calendarOptions = {
            defaultView: 'month',
            height: 550,
            allDaySlot: false,
            selectable: false,
            editable: false,
            selectHelper: false,
            selectOverlap: false,
            eventOverlap: false,
            hiddenDays: [0, 6],
            minTime: '06:30',
            maxTime: '21:00',
            columnFormat: 'ddd',
            eventConstraint: 'businessHours',
            timezone: 'local',
            businessHours: {
                start: '07:00',
                end: '20:30',
            },
            eventRender: eventHandlers.eventRender,
            header: {
                left: 'title',
                center: 'agendaWeek month',
                right: 'today prev,next',
            },
        };

        /* Event Handlers */
        $scope.assignTutorial = function (tutor, tutorialId) {
            var event = findEventByTutorialId(tutorialId);
            if (tutor.id === -1) {
                event.tutorId = -1;
                TutorialService.clearTutorialAssignment(tutorialId);
                recolorEvent(event, COLORS.list[0]);
            } else {
                event.tutorId = tutor.id;
                TutorialService.assignTutorial(tutorialId, tutor.id);
                recolorEvent(event, tutor.color.val);
            }

        };

        $scope.assignGroup = function (tutor, groupId) {
            if (tutor.id === -1) {
                TutorialGroupService.clearTutorialGroupAssignment(groupId);
                $scope.events.some(function (evt) {
                    evt.tutorId = -1;
                    recolorEvent(evt, COLORS.list[0]);
                });
            } else {
                TutorialGroupService.assignTutorialGroup(groupId, tutor.id);
                $scope.events.some(function (evt) {
                    evt.tutorId = tutor.id;
                    recolorEvent(evt, tutor.color.val);
                });
            }
        };

        //Selects a unit, loads the tutorial groups for that unit.
        $scope.selectUnit = function (unit) {
            if (unit) {
                $scope.selectedUnit = unit;

                promises.push(TutorPreferenceService.getTutorPreferencesForUnit(unit.id).then(
                    function successCallback(response) {
                        $scope.tutors = response.data;
                        $scope.tutors.some(function (tutor, idx) {
                            tutor.color = COLORS.list[idx % COLORS.list.length];
                        });
                        $scope.tutors.push({
                            id: unit.unitCoordinator.id,
                            name: "Unit Coordinator",
                            course: "Drag this onto events or groups assign the unit coordinator.",
                            color: COLORS.unitCoordinator,
                        });
                        $scope.tutors.push({
                            id: -1,
                            name: "Unassign",
                            course: "Drag this onto events or groups to clear the current assignment.",
                            color: COLORS.calendar,
                        });
                    }
                ));

                promises.push(TutorialGroupService.loadTutorialGroupsForUnit(unit.id).then(
                    function successCallback(response) {
                        $scope.groups = response.data;

                    }
                ));

                //After both tutors and tutorials have been loaded, select a group to display.
                $q.all(promises).then(
                    function successCallback() {
                        if ($scope.groups.length > 0) {
                            $scope.selectGroup($scope.groups[0]);
                        } else {
                            $scope.selectedGroup = {
                                groupName: "No Groups"
                            };
                        }
                    }
                );
            }
        };

        //Sets the selected group (dictates which tutorials are displayed)
        $scope.selectGroup = function (group) {
            if (group) {
                $scope.selectedGroup = group;
                $scope.events.length = 0;
                angular.copy(TransformService.transformTutorialDTOsToJS(group.tutorials), $scope.events);

                //Timeout allows calendar to add internal events from newly added events.
                $timeout(colorEvents, 5);
            }
        };

        $scope.expandTutor = function (tutor) {
            if (isDragging) {
                isDragging = false;
            } else {
                tutor.expanded = !tutor.expanded;
            }
        };

    }
]);
