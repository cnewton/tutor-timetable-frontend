'use strict';

angular.module('tutor-timetable')

.controller('TutorApplicationController', [
    '$scope',
    '$state',
    'TutorService',
    'TutorPreferenceService',
    'UnitService',
    'TutorApplicationService',
    'TransformService',
    '$q',
    '$uibModal',
    'tutorPromise',
    function ($scope, $state, TutorService, TutorPreferenceService, UnitService, TutorApplicationService, TransformService, $q, $uibModal, tutorPromise) {

        var applicationPromises = [];
        $scope.application = TransformService.transformTutorApplicationFields(tutorPromise.data);
        $scope.events = TransformService.transformAvailabiltiesToJS($scope.application.availabilities);

        // Application is retrieved from parent scope.
        $scope.unitSources = [];
        $scope.selectedUnits = [];

        //Construct the unit list with already selected units.
        UnitService.all().then(
            function successCallback(response) {
                var viewUnits = TutorApplicationService.getUnitSelectionList(response.data, $scope.application.preferredUnits);
                Array.prototype.push.apply($scope.unitSources, viewUnits);
            }
        );

        /* Event handlers */
        $scope.addFailedUnit = function () {
            //If not empty.
            if ($scope.application.failedUnit) {
                var pos = $scope.application.failedUnits.indexOf($scope.application.failedUnit);
                //If not already included
                if (pos === -1) {
                    $scope.application.failedUnits.push($scope.application.failedUnit);
                    $scope.application.failedUnit = "";
                }
            }
        };
        $scope.removeFailedUnit = function (unit) {
            var pos = $scope.application.failedUnits.indexOf(unit);
            if (pos !== -1) {
                $scope.application.failedUnits.splice(pos, 1);
            }
        };
        //Allows enter to submit failed units.
        $scope.keyFail = function (event) {
            if (event.keyCode === 13) {
                $scope.addFailedUnit();
            }
        };


        $scope.submitApplication = function () {
            var additionUnits = TutorApplicationService.getPreferredUnitAdditions($scope.selectedUnits, $scope.application.preferredUnits);
            var subtractionUnits = TutorApplicationService.getPreferredUnitSubtractions($scope.selectedUnits, $scope.application.preferredUnits);
            var additionAvailabilities = TutorApplicationService.getAvailabilityAdditions($scope.events, TransformService.transformAvailabiltiesToJS($scope.application.availabilities));
            var subtractionAvailabilities = TutorApplicationService.getAvailabilitySubtractions($scope.events, TransformService.transformAvailabiltiesToJS($scope.application.availabilities));
            var applicationDetails = TutorApplicationService.getApplicationDetails($scope.application);

            applicationPromises.push(TutorService.updateTutorDetails($scope.application.id, applicationDetails));

            if (additionUnits.length > 0) {
                applicationPromises.push(TutorPreferenceService.addUnitPreferenceList($scope.application.id, additionUnits));
            }
            if (subtractionUnits.length > 0) {
                applicationPromises.push(TutorPreferenceService.removeUnitPreferenceList($scope.application.id, subtractionUnits));
            }
            if (additionAvailabilities.length > 0) {
                applicationPromises.push(TutorService.addAvailabilityList($scope.application.id, TransformService.transformAvailabiltiesToJava(additionAvailabilities)));
            }
            if (subtractionAvailabilities.length > 0) {
                applicationPromises.push(TutorService.removeAvailabilityList($scope.application.id, subtractionAvailabilities));
            }

            var modal = $uibModal.open({
                template: 'Please Wait...',
                backdrop: 'static',
                keyboard: false,
            });
            $q.all(applicationPromises).then(function successCallback() {
                //Remove unnecessary post material.
                $scope.application.preferredUnits = null;
                $scope.application.availabilities = null;
                $state.reload();
                modal.dismiss();
            });
        };

}]);
