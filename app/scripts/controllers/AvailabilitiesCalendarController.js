'use strict';

angular.module('tutor-timetable')

.controller('AvailabilitiesCalendarController', [
    '$scope',
    'uiCalendarConfig',
    function ($scope, uiCalendarConfig) {

        $scope.calendarSources = [$scope.events];

        //Local Helper Functions
        var tryCombineEvents = function (newEvent) {
            var combined = false;
            $scope.events.some(function (oldEvent, idx) {
                //if (oldEvent.end.getHours() === newEvent.start.getHours() && oldEvent.end.getMinutes() === newEvent.start.getMinutes()) {
                if (oldEvent.end._d.valueOf() === newEvent.start._d.valueOf()) {
                    oldEvent.end = newEvent.end;
                    $scope.events.splice($scope.events.indexOf(newEvent), 1);
                    oldEvent.id = 0;
                    newEvent = oldEvent;
                    combined = true;
                    return true;
                    //} else if (newEvent.end.getHours() === oldEvent.start.getHours() && newEvent.end.getMinutes() === oldEvent.start.getMinutes()){
                } else if (newEvent.end._d.valueOf() === oldEvent.start._d.valueOf()) {
                    newEvent.end = oldEvent.end;
                    newEvent.id = 0;
                    $scope.events.splice(idx, 1);
                    combined = true;
                    return true;
                }
            });
            if (combined) {
                tryCombineEvents(newEvent);
            }
        };

        /* Calendar Event Handler Functions */
        var calendarEventHandlers = {

            /* On Selection Completion */
            select: function (start, end) {
                var newEvent = {
                    title: 'Available',
                    start: start,
                    end: end,
                    id: 0,
                };
                $scope.events.push(newEvent);
                tryCombineEvents(newEvent);
            },

            /* On event click (select event) */
            eventClick: function (event) {
                if ($scope.selectedEvent) {
                    $scope.selectedEvent.color = '';
                    uiCalendarConfig.calendars.calendar.fullCalendar('updateEvent', $scope.selectedEvent);
                }
                event.color = 'RED';
                uiCalendarConfig.calendars.calendar.fullCalendar('updateEvent', event);
                $scope.selectedEvent = event;
            },

            /* On clear button press */
            clearEvents: function () {
                uiCalendarConfig.calendars.calendar.fullCalendar('removeEvents');
                $scope.events.length = 0;
            },

            /* On remove button press */
            removeEvent: function () {
                uiCalendarConfig.calendars.calendar.fullCalendar('removeEvents', [$scope.selectedEvent._id]);
                $scope.events.some(function (event, idx) {
                    if ($scope.selectedEvent._id === event._id) {
                        $scope.events.splice(idx, 1);
                        return true;
                    }
                });
            },

            /* On event drag & drop */
            eventDrop: function (event) {
                var droppedEvent;
                $scope.events.some(function (evt) {
                    droppedEvent = evt;
                    if (evt._id === event._id) {
                        evt.end = event.end;
                        evt.start = event.start;
                        evt.id = 0;
                        tryCombineEvents(evt);
                        return true;
                    }
                });
                //If there was a combine event & The dragged event was deleted
                if ($scope.events.indexOf(droppedEvent) === -1) {
                    uiCalendarConfig.calendars.calendar.fullCalendar('removeEvents', [event._id]);
                }
            },

            /* On event resize */
            eventResize: function (event) {
                $scope.events.some(function (evt) {
                    if (evt._id === event._id) {
                        evt.end = event.end;
                        evt.start = event.start;
                        evt.id = 0;
                        tryCombineEvents(evt);
                        return true;
                    }
                });
            },
        };

        /* Calendar setup options */
        $scope.calendarOptions = {
            defaultView: 'agendaWeek',
            height: 550,
            allDaySlot: false,
            selectable: true,
            editable: true,
            selectHelper: false,
            selectOverlap: false,
            eventOverlap: false,
            hiddenDays: [0, 6],
            minTime: '06:30',
            maxTime: '21:00',
            columnFormat: 'ddd',
            eventConstraint: 'businessHours',
            selectConstraint: 'businessHours',
            timezone: 'local',
            select: calendarEventHandlers.select,
            eventClick: calendarEventHandlers.eventClick,
            eventDrop: calendarEventHandlers.eventDrop,
            eventResize: calendarEventHandlers.eventResize,
            businessHours: {
                start: '07:00',
                end: '20:30',
            },
            header: {
                left: 'clearButton',
                center: '',
                right: 'removeButton',
            },
            customButtons: {
                clearButton: {
                    text: 'Clear',
                    click: calendarEventHandlers.clearEvents,
                },
                removeButton: {
                    text: 'Remove',
                    click: calendarEventHandlers.removeEvent,
                },
            },
        };

    }
]);
