'use strict';

angular.module('tutor-timetable')

.controller('AdministrationController', [
    '$scope',
    '$state',
    'AdministrationService',
    '$window',
    '$uibModal',
    'TransformService',
    'uiDatetimePickerConfig',
    function ($scope, $state, AdministrationService, $window, $uibModal, TransformService, uiDatetimePickerConfig) {

        //Config required to have the datetimepicker appear next to the clicked cell.
        uiDatetimePickerConfig.appendToBody = true;
        uiDatetimePickerConfig.buttonBar = false;

        //Local Helper functions
        var openModel = function (message) {
            return $uibModal.open({
                template: message,
            });
        };

        var modalPromiseHandler = function (promise) {
            var modal = openModel("Working...");
            promise.then(function successCallback() {
                    modal.dismiss();
                },
                function errorCallback(response) {
                    modal.dismiss();
                    modal = openModel("There were issues. " + response.data);
                }
            );
        };

        AdministrationService.getSystemState().then(
            function success(response) {
                $scope.systemState = response.data;
                TransformService.transformSystemStateDatesToJS($scope.systemState);
            }
        );

        /* Event Handlers */

        $scope.toggleTutorAccounts = function () {
            $scope.systemState.tutorsLocked = !($scope.systemState.tutorsLocked);
            modalPromiseHandler(AdministrationService.toggleTutorAccounts());
        };

        $scope.toggleCoordinatorAccounts = function () {
            $scope.systemState.coordinatorsLocked = !$scope.systemState.coordinatorsLocked;
            modalPromiseHandler(AdministrationService.toggleCoordinatorAccounts());
        };

        $scope.deleteAllUnits = function () {
            if ($window.prompt("Enter 'DELETE' to continue") === "DELETE") {
                modalPromiseHandler(AdministrationService.deleteAllUnits());
            }
        };

        $scope.deleteAllTutorials = function () {
            if ($window.prompt("Enter 'DELETE' to continue") === "DELETE") {
                modalPromiseHandler(AdministrationService.deleteAllTutorials());
            }
        };

        $scope.deleteAllTutorApplications = function () {
            if ($window.prompt("Enter 'DELETE' to continue") === "DELETE") {
                modalPromiseHandler(AdministrationService.deleteAllTutorApplications());
            }
        };

        $scope.deleteAllTutorAssignments = function () {
            if ($window.prompt("Enter 'DELETE' to continue") === "DELETE") {
                modalPromiseHandler(AdministrationService.deleteAllTutorAssignments());
            }
        };

        $scope.downloadUnitCsv = function () {
            AdministrationService.downloadUnitCsv();
        };

        $scope.downloadTutorialCsv = function () {
            AdministrationService.downloadTutorialCsv();
        };

        $scope.assignTutors = function () {
            modalPromiseHandler(AdministrationService.assignTutors());
        };

        $scope.updateSemesterStart = function () {
            AdministrationService.setSemesterStartDate(TransformService.transformDateToJava($scope.systemState.semesterStart));
        };

        $scope.updateSemesterEnd = function () {
            AdministrationService.setSemesterEndDate(TransformService.transformDateToJava($scope.systemState.semesterEnd));
        };

        $scope.updateApplicationStart = function () {
            AdministrationService.setApplicationStartDate(TransformService.transformDateToJava($scope.systemState.applicationStart));
        };

        $scope.updateApplicationEnd = function () {
            AdministrationService.setApplicationEndDate(TransformService.transformDateToJava($scope.systemState.applicationEnd));
        };

        $scope.updateLockout = function () {
            AdministrationService.setLockoutDate(TransformService.transformDateToJava($scope.systemState.lockout));
        };

        $scope.updateTutorMaxHours = function () {
            if ($scope.systemState.tutorMaxHours) {
                AdministrationService.setTutorMaxHours($scope.systemState.tutorMaxHours);
            }
        };
                }
                ]);
