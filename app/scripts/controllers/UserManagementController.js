'use strict';

angular.module("tutor-timetable")

.controller("UserManagementController", [
    '$scope',
    'UserService',
    '$window',
    function ($scope, UserService, $window) {

        $scope.userDetailsState = {
            hasError: false,
            error: "",
        };

        UserService.all().then(
            function successCallback(response) {
                $scope.users = response.data;
                if ($scope.users) {
                    $scope.selectedUser = $scope.users[0];
                }
            }
        );

        /*Local helper functions*/

        var setNewAccountType = function () {
            var newUser = null;
            if ($scope.selectedUser.accountType === 'ADMIN') {
                newUser = {
                    "@class": "Administrator"
                };
            } else if ($scope.selectedUser.accountType === 'COORDINATOR') {
                newUser = {
                    "@class": "Coordinator"
                };
            } else if ($scope.selectedUser.accountType === 'TUTOR') {
                newUser = {
                    "@class": "Tutor"
                };
            } else {
                $scope.userDetailsState.hasError = true;
                $scope.userDetailsState.error = "Please select an account type";
            }
            return newUser;
        };

        /*Event handlers*/

        $scope.updateUser = function (form) {
            $scope.userDetailsState.hasError = false;

            var user = $scope.selectedUser;
            if (form.$valid) {

                //Password update handling
                if (user.password) {
                    if (user.password !== user.repeatPassword) {
                        $scope.userDetailsState.hasError = true;
                        $scope.userDetailsState.error = "New passwords do not match";
                    } else {
                        UserService.adminUpdatePassword(user.id, user.password).then(
                            function successCallback() {},
                            function errorCallback(response) {
                                $scope.userDetailsState.hasError = true;
                                $scope.userDetailsState.error = response.data;
                            }
                        );
                    }
                }

                //Details update handling
                UserService.update(user).then(
                    function successCallback(response) {
                        var arrayPos = $scope.users.indexOf(user);
                        $scope.users[arrayPos] = response.data;
                    },
                    function errorCallback(response) {
                        $scope.userDetailsState.hasError = true;
                        $scope.userDetailsState.error = response.data;
                    }
                );
            }
        };

        $scope.createUser = function (form) {

            $scope.userDetailsState.hasError = false;

            if (form.$valid) {
                var user = setNewAccountType();

                if (user) {
                    if ($scope.selectedUser.password !== $scope.selectedUser.repeatPassword) {
                        $scope.userDetailsState.hasError = true;
                        $scope.userDetailsState.error = "Passwords did not match";
                    } else {
                        user.username = $scope.selectedUser.username;
                        user.name = $scope.selectedUser.name;
                        user.email = $scope.selectedUser.email;
                        user.mobile = $scope.selectedUser.mobile;
                        user.staffId = $scope.selectedUser.staffId;
                        user.password = $scope.selectedUser.password;
                        UserService.create(user).then(
                            function successCallback(response) {
                                $scope.users.push(response.data);
                                $scope.selectedUser = response.data;
                                $scope.creating = false;
                                form.$submitted = false;
                            },
                            function errorCallback(response) {
                                $scope.userDetailsState.hasError = true;
                                $scope.userDetailsState.error = response.data;
                            }
                        );
                    }
                }
            }
        };

        $scope.newUser = function () {
            $scope.selectedUser = {
                name: "New User"
            };
            $scope.creating = true;
        };

        $scope.selectUser = function (user) {
            $scope.creating = false;
            $scope.selectedUser = user;
        };


        $scope.deleteUser = function (user) {
            if ($window.confirm("Are you sure you want to delete this?")) {
                var arrayPos = $scope.users.indexOf(user);
                $scope.users.splice(arrayPos, 1);
                UserService.delete(user.id);
            }
        };
}]);
