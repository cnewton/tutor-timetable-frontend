'use strict';

// Click to navigate
// similar to <a href="#/partial"> but hash is not required,
// e.g. <div page-link="/partial">

angular.module('tutor-timetable')
    .directive('pageLink', ['$location', function ($location) {
        return {
            link: function (scope, element, attrs) {
                element.on('click', function () {
                    scope.$apply(function () {
                        $location.path(attrs.pageLink);
                    });
                });
            }
        };
    }]);
