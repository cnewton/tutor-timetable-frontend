'use strict';

angular.module('tutor-timetable')

.directive('availabilitiesCalendar', [
    function () {
        return {
            restrict: "E",
            scope: {
                events: "=",
            },
            templateUrl: "view/availabilities-calendar.html",
            controller: "AvailabilitiesCalendarController",
        };
}]);
