'use strict';

angular.module('tutor-timetable')

.directive('tutorEmail', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$validators.tutorEmail = function (modelValue, viewValue) {
                if (ctrl.$isEmpty(modelValue)) {
                    // consider empty models to be valid
                    return true;
                }

                //Tutor email must not be curtin email.
                if (viewValue.indexOf("curtin.edu.au") !== -1) {
                    // it is invalid
                    return false;
                } else {
                    // it is valid
                    return true;
                }
            };
        }
    };
});
